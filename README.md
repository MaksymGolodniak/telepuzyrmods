# **Installation**

Paste mods to [minecraft folder]/mods

True for client + server mods | False for only client mods 
## List of mods

| № | Name | Is need for server | file name | Link |
| --- | --- | --- | --- | --- |
| 1. | Just Enough Items (JEI) | True | jei-1.18.2-forge-10.2.1.1002.jar | <https://www.curseforge.com/minecraft/mc-mods/jei> |
| 2. | Twilight forest | True | twilightforest-1.18.2-4.1.1494-universal | <https://www.curseforge.com/minecraft/mc-mods/the-twilight-forest> |
| 3. | Oh The Biomes You'll Go | True | Oh_The_Biomes_You'll_Go-forge-1.18.2-1.4.7 | <https://www.curseforge.com/minecraft/mc-mods/oh-the-biomes-youll-go> |
| 4. | TerraBlender (Forge) (Core for "Oh The Biomes You'll Go") | True | TerraBlender-forge-1.18.2-1.2.0.126 | <https://www.curseforge.com/minecraft/mc-mods/terrablender> |
| 5. | Iron Chests | True | ironchest-1.18.2-13.2.11 | <https://www.curseforge.com/minecraft/mc-mods/iron-chests> |
| 6. | JourneyMap | False | journeymap-1.18.2-5.9.3-forge | <https://www.curseforge.com/minecraft/mc-mods/journeymap> |
| 7. | Cyclic | True | Cyclic-1.18.2-1.7.14 | <https://www.curseforge.com/minecraft/mc-mods/cyclic> |
| 8. | Bookshelf (Core for Cyclic and Enchantment Descriptions) | True | Bookshelf-Forge-1.18.2-13.2.52 | <https://www.curseforge.com/minecraft/mc-mods/bookshelf> |
| 9. | Enchantment Descriptions | False| EnchantmentDescriptions-Forge-1.18.2-10.0.12 | <https://www.curseforge.com/minecraft/mc-mods/enchantment-descriptions> |
| 10. | Curios API (Forge) (Core for Cyclic and Botania) | True | curios-forge-1.18.2-5.0.9.0 | <https://www.curseforge.com/minecraft/mc-mods/curios> |
| 11. | CraftTweaker (Core for Cyclic) | True | CraftTweaker-forge-1.18.2-9.1.205 | <https://www.curseforge.com/minecraft/mc-mods/crafttweaker> |
| 12. | JEITweaker (compatibility for jei and crafttweaker) | True | JEITweaker-1.18.2-3.0.0.9 | <https://www.curseforge.com/minecraft/mc-mods/jeitweaker> |
| 13. | Patchouli (Core for Cyclic and Botania) | True | Patchouli-1.18.2-71.1 | <https://www.curseforge.com/minecraft/mc-mods/patchouli> |
| 14. | Distant Horizons: A Level of Detail mod | False | DistantHorizons-1.6.5a-1.18.2.jar | <https://www.curseforge.com/minecraft/mc-mods/distant-horizons/> |
| 15. | Create | True | create-1.18.2-0.5.0.i.jar | <https://www.curseforge.com/minecraft/mc-mods/create> |
| 16. | Macaw's Bridges | True | mcw-bridges-2.0.7-mc1.18.2forge.jar | <https://www.curseforge.com/minecraft/mc-mods/macaws-bridges> |
| 17. | Mouse Tweaks | False | MouseTweaks-forge-mc1.18-2.21.jar | <https://www.curseforge.com/minecraft/mc-mods/mouse-tweaks> |
| 18. | Carry On | True | carryon-1.18.2-1.17.0.8.jar | <https://www.curseforge.com/minecraft/mc-mods/carry-on> |
| 19. | Flywheel (Required for Create) | True | flywheel-forge-1.18.2-0.6.8.a.jar | <https://www.curseforge.com/minecraft/mc-mods/flywheel> |
| 20. | Ice and Fire (Deleted, Not compatibility with Multiverse-Core) | True | iceandfire-2.1.12-1.18.2-beta | <https://www.curseforge.com/minecraft/mc-mods/ice-and-fire-dragons> |
| 21. | Citadel Mod (Core for Ice and Fire and Alex Mobs) | True | citadel-1.11.3-1.18.2 | <https://www.curseforge.com/minecraft/mc-mods/citadel> |
| 22. | Industrial Craft Reborn | True | indreb-1.18.2-0.13.0 | <https://www.curseforge.com/minecraft/mc-mods/industrial-reborn> |
| 23. | Forbbiden and Arcanus | True | forbidden_arcanus-1.18.2-2.1.2 | <https://www.curseforge.com/minecraft/mc-mods/forbidden-arcanus/files/4457443> |
| 24. | Valhelsia Core ( Core for Forbbiden and Arcanus and Valhelsia Structures) | True | valhelsia_core-1.18.2-0.3.1 | <https://www.curseforge.com/minecraft/mc-mods/valhelsia-core> |
| 25. | Valhelsia Structures | True | valhelsia_structures-forge-1.18.2-0.1.0 | <https://www.curseforge.com/minecraft/mc-mods/valhelsia-structures>
| 26. | Botania | True | Botania-1.18.2-435 | <https://www.curseforge.com/minecraft/mc-mods/botania> |
| 27. | Mowzie's Mobs | True | mowziesmobs-1.5.32 | <https://www.curseforge.com/minecraft/mc-mods/mowzies-mobs> |
| 28. | Geckolib Core ( Core For Mowzie Mobs ) | True | geckolib-forge-1.18-3.0.57 | <https://www.curseforge.com/minecraft/mc-mods/geckolib> |
| 29. | Waystones | True | waystones-forge-1.18.2-10.2.0 | <https://www.curseforge.com/minecraft/mc-mods/waystones> |
| 30. | Balm Core (Core For Waystones) | True | balm-3.2.6 | <https://www.curseforge.com/minecraft/mc-mods/balm> |
| 31. | When Dungeons Arise | True | DungeonsArise-1.18.2-2.1.52-release | <https://www.curseforge.com/minecraft/mc-mods/when-dungeons-arise> |
| 32. | Alex mobs | True | alexsmobs-1.18.6 | <https://www.curseforge.com/minecraft/mc-mods/alexs-mobs> |
| 33. | Alex mobs EXTRA music mod | False (maybe) | Alex's Mobs Music Mod | <https://www.curseforge.com/minecraft/mc-mods/alexs-mobs-battle-music> |
| 34. | Aquamirae | True | aquamirae-5.api10 | <https://www.curseforge.com/minecraft/mc-mods/ob-aquamirae> |
| 35. | Obscure API (Core of Aquamirae) | True | obscure_api-10 | <https://www.curseforge.com/minecraft/mc-mods/obscure-api> |
| 36. | Aquamirae EXTRA music mod | False (maybe) | Aquamirae Mod EXTRA Music 1.18 | <https://www.curseforge.com/minecraft/mc-mods/aquamirae-mod-music> |
| 37. | Additional structures | True | Rex's-AdditionalStructures-1.18.2-(v.3.1.1) | <https://www.curseforge.com/minecraft/mc-mods/additional-structures> |
| 38. | Ars Nouveau | True | ars_nouveau-1.18.2-2.8.0 | <https://www.curseforge.com/minecraft/mc-mods/ars-nouveau> |
| 39. | Quark | True | Quark-3.2-358 | <https://beta.curseforge.com/minecraft/mc-mods/quark> |
| 40. | AutoRegLib ( Core for Quark) | True | AutoRegLib-1.7-53 | <https://beta.curseforge.com/minecraft/mc-mods/autoreglib> |
| 41. | Tinker Construct | True | TConstruct-1.18.2-3.6.3.111 | <https://www.curseforge.com/minecraft/mc-mods/tinkers-construct> |
| 42. | Mantle (Core For Tinker Construct) | True | Mantle-1.18.2-1.9.43 | <https://www.curseforge.com/minecraft/mc-mods/mantle> |
| 43. | Reliquary | True | reliquary-1.18.2-2.0.19.1161 | <https://www.curseforge.com/minecraft/mc-mods/reliquary-v1-3> |
| 44. | Jade | False (maybe) | Jade-1.18.2-forge-5.2.6 | <https://www.curseforge.com/minecraft/mc-mods/jade> |
| 45. | Optifine | False | preview_OptiFine_1.18.2_HD_U_H9_pre3 | <https://optifine.net/downloads> |
| 46. | Better FPS | IDK | betterfpsdist-1.18.2-1.5 | <https://www.curseforge.com/minecraft/mc-mods/betterfps> |
| 47. | Traveler Title | False | TravelersTitles-1.18.2-Forge-2.1.1 | <https://www.curseforge.com/minecraft/mc-mods/travelers-titles> |
| 48. | YUNG's API ( Core for Traveler Title )| False | YungsApi-1.18.2-Forge-2.2.9 | <https://www.curseforge.com/minecraft/mc-mods/yungs-api> |
| 49. | Farmer Delight | True | FarmersDelight-1.18.2-1.2.0 | <https://www.curseforge.com/minecraft/mc-mods/farmers-delight> |
| 50. | Crated Vanilla Food (AddOn for Farmer Delight)| True | Crated_Vanilla_Foods_1.1-1.18.2 | <https://www.curseforge.com/minecraft/mc-mods/crated-foods-farmers-delight> |
| 51. | Honey Expansion (AddOn For Farmer Delight) | True | honeyexpansion-1.1.1 | <https://www.curseforge.com/minecraft/mc-mods/honey-expansion-add-on-for-farmers-delight> |
| 52. | Twilight Decor Mod (AddOn for Twilight forest, Farmer Delight and Quark)| True | twilightdecor-1.18.2-v1.1 | <https://www.curseforge.com/minecraft/mc-mods/twilightdecor> |
| 53. | Corail Woodcutter | True | corail_woodcutter-1.18.2-2.3.9 | <https://www.curseforge.com/minecraft/mc-mods/corail-woodcutter> | 
| 54. | Blueprint ( Core for Twilight Decor Mod) | True | blueprint-1.18.2-5.5.0 | <https://www.curseforge.com/minecraft/mc-mods/blueprint> |
| 55. | Decorative Blocks (Also required mod for Twilight Decor Mod) | True | Decorative Blocks-forge-1.18.2-2.1.2 | <https://www.curseforge.com/minecraft/mc-mods/decorative-blocks/> |
| 56. | Aquaculture | True | Aquaculture-1.18.2-2.3.11 | <https://www.curseforge.com/minecraft/mc-mods/aquaculture> |
| 57. | Falling Tree | True | FallingTree-1.18.2-3.5.5 | <https://www.curseforge.com/minecraft/mc-mods/falling-tree>